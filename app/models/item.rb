class Item < ApplicationRecord
  belongs_to :item_type
  belongs_to :survivor

  validates_presence_of :quantity
end
