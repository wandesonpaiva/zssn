class Survivor < ApplicationRecord

  has_one :last_location
  has_many :items

  validates_presence_of :name, :age, :gender, :last_location #, :is_infected


  def update_last_location(latitude, longitude)
    self.last_location.latitude = latitude
    self.last_location.longitude = longitude
    self.last_location.save
  end

  def mark_as_infected
    self.is_infected = true
    self.save
  end

end
