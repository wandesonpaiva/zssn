class CreateLastLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :last_locations do |t|
      t.decimal :latitude
      t.decimal :longitude
      t.references :survivor, foreign_key: true
      t.timestamps
    end
  end
end
