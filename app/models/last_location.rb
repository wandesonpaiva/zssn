class LastLocation < ApplicationRecord
  belongs_to :survivor, required: false

  validates_presence_of :latitude, :longitude

end
